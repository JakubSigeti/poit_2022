from threading import Lock
from flask import Flask, render_template, session, request, jsonify, url_for
from flask_socketio import SocketIO, emit, send, disconnect  
import time
import random
import math
import serial
import matplotlib.pyplot as plt
import numpy as np
import json


async_mode = None

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
thread = None
thread_lock = Lock()

ser=serial.Serial("/dev/ttyS0",9600)
ser.baudrate = 9600

def write_to_file(index, value):
    file = open("static/files/test.txt","a+")
    row = json.dumps({"index":index, "value":value})
    file.write("%s\r\n" %row)


def background_thread(args):
    count = 0
    dataList = []
    read_ser=ser.readline()
    y=[]
    while True:
        if args:
          relay1 = dict(args).get('relay1')
          relay2 = dict(args).get('relay2')
          relay3 = dict(args).get('relay3')
          relay4 = dict(args).get('relay4')
          connected = dict(args).get('connected')
          #btnV = dict(args).get('btn_value')
          #sliderV = dict(args).get('slider_value')
        else:
          relay1 = 0
          relay2 = 0
          relay3 = 0
          relay4 = 0
          connected = 0
          #btnV = 'null'
          #sliderV = 0
        #print(A)
        print(args)
        #socketio.sleep(2)
        
        
        #command = channel
        if relay1 == 1:
           command = '1+'
        else:
            command = '1-'
        ser.write(command.encode())
        
        if relay2 == 1:
           command = '2+'
        else:
            command = '2-'
        ser.write(command.encode())
        
        if relay3 == 1:
           command = '3+'
        else:
            command = '3-'
        ser.write(command.encode())
        
        if relay4 == 1:
           command = '4+'
        else:
            command = '4-'
        
        ser.write(command.encode())
        
        y.append(float(read_ser))
        
        write_to_file(count, y[-1])
        #t=np.arange(0,len(y))
        
        dataDict = {
          "t": time.time(),
          "x": count,
          "y": y[-1]}
        dataList.append(dataDict)
        #if len(dataList)>0:
        #  print(str(dataList))
        #  print(str(dataList).replace("'", "\""))
        if connected:
            count += 1
            socketio.emit('my_response',
                          {'data': y[-1], 'count': count},
                          namespace='/test')
        read_ser=ser.readline()
        
        
@app.route('/')
def index():
    return render_template('index.html', async_mode=socketio.async_mode)

@app.route('/read/<int:num>')
def read_from_file(num):
    file = open("static/files/test.txt","r")
    rows = file.readlines()
    #return {'rows':rows[-num:]}
    #retval = {"rows":rows[-num:]}
    #return json.dumps(retval)
    return '{"rows":['+','.join(rows[-num:])+']}'
    #return '<br>'.join(rows[-num:])

@socketio.on('my_event', namespace='/test')
def test_message(message):   
#    session['receive_count'] = session.get('receive_count', 0) + 1 
    session['connected'] = message['value']
    
 #   if message['value'] == 0:
 #       emit('disconnected', {connected: message['value']})
#    emit('my_response',
#         {'data': message['value'], 'count': session['receive_count']})
 
@socketio.on('relay', namespace='/test')
def set_relay(message):   
#    session['receive_count'] = session.get('receive_count', 0) + 1 
    #session['A'] = message['value']
    #print(message)  
    channel = str(message['channel'])
    state = message['state']
    session['relay'+channel]=state
    #print(session)  
    #command = channel
    #if state == 1:
    #    command = command + '+'
    #else:
    #    command = command + '-'
    #print('Command: ' + command + "\n")
    #ser.write(command.encode())
    

#    emit('my_response',
#         {'data': message['value'], 'count': session['receive_count']}) 
 
@socketio.on('disconnect_request', namespace='/test')
def disconnect_request():
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': 'Disconnected!', 'count': session['receive_count']})
    disconnect()

@socketio.on('connect', namespace='/test')
def test_connect():
    global thread
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(target=background_thread, args=session._get_current_object())
#    emit('my_response', {'data': 'Connected', 'count': 0})

@socketio.on('click_event', namespace='/test')
def db_message(message):   
    session['btn_value'] = message['value']    

if __name__ == '__main__':
    socketio.run(app, host="0.0.0.0", port=80, debug=True)
var gauge;
var state = [0,0,0,0,0];
var socket;
var x = new Array();
var y = new Array();
var max_graph_items = 20;

function createGauge(){
	gauge = new RadialGauge({
		renderTo: 'canvasID',
		width: 400,
		height: 400,
		units: "Movement detected",
		minValue: 0,
		maxValue: 1,
		majorTicks: [
			"NO",
			"|",
			"YES"
		],
		minorTicks: 0,
		strokeTicks: true,
		highlights: [
		{
			"from": 0,
			"to": 0.5,
			"color": "rgba(200, 50, 50, .75)"
		},
		{
			"from": 0.5,
			"to": 1,
			"color": "rgba(50, 200, 50, .75)"
		}
		],
		colorPlate: "#ddd",
		borderShadowWidth: 1,
		borders: true,
		needleType: "arrow",
		needleWidth: 2,
		needleCircleSize: 7,
		needleCircleOuter: true,
		needleCircleInner: false,
		animationDuration: 400,
		animationRule: "linear"
	});
	gauge.draw();
	gauge.value = "0";
}


function onRelay(id){
	//console.log('state: ', state);
	var selector='#relay'+id;
	
	if(state[id] === 0){
		state[id] = 1;
		$(selector).addClass('on').removeClass('off');
	} else{
		state[id] = 0;
		$(selector).addClass('off').removeClass('on');
	}
	
	//alert(id + ':' + state);
	//console.log('new state: ', state);
	socket.emit('relay', {channel: id, state: state[id]});
}

function addLog(index,value){
	$('#log').prepend('Received #'+index+': '+value+'<br>').html(); 
}

function addToGraph(index,value){
	x.push(parseFloat(index));
	y.push(parseFloat(value));
	x=x.slice(-max_graph_items);
	y=y.slice(-max_graph_items);
	
	var trace = {
		x: x,
		y: y,
		name: 'Graf y'
	}; 

	var layout = {
		title: 'Sensor history <br>Number of last samples: '+max_graph_items,
		xaxis: {
			title: 'Sample',
		},
		yaxis: {
			range: [-0.1, 1.1],
			title: 'Value',
		}
	};
	
	var traces = new Array();
	traces.push(trace);

	Plotly.newPlot($('#plotdiv')[0], traces, layout);
}

function setReadGraph(x,y){
	
	var trace = {
		x: x,
		y: y,
		name: 'Graf y'
	}; 

	var layout = {
		title: 'Data read from file',
		xaxis: {
			title: 'Sample',
		},
		yaxis: {
			range: [-0.1, 1.1],
			title: 'Value',
		}
	};
	
	var traces = new Array();
	traces.push(trace);

	Plotly.newPlot($('#plotread')[0], traces, layout);

}

function setGauge(value){
	gauge.value = value;
}

function readFile(index, value){
	$('#data').prepend('Read #'+index+': '+value+'<br>').html(); 
}

function onData(index,value){
	setGauge(value);
	addLog(index,value);
	addToGraph(index,value);
}

function openSocket(){
	namespace = '/test';
	socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port + namespace);

	socket.on('connect', function() {
		socket.emit('my_event', {data: 'I\'m connected!', value: 1}); 
		/*socket.on('disconnected', function(msg) {
			stopListening();
			socket.disconnect();      
		});*/
	});
}

function startListening(){
	socket.on('my_response', function(msg) {
		onData(msg.count,msg.data);                
	});
	
}

function stopListening(){
	socket.off('my_response');
}


function closeSocket(){
	socket.emit('my_event', {data: 'I\'m disconnected!', value: 0});
	stopListening();
	socket.disconnect();
}


function initButtons(){
	$('form#emit').submit(function(event) {
		socket.emit('my_event', {value: $('#emit_value').val()});
		return false; 
	});
	$('#buttonVal').click(function(event) {
		socket.emit('click_event', {value: $('#buttonVal').val()});
		return false; 
	});
	$('form#disconnect').submit(function(event) {
		socket.emit('disconnect_request');
		return false; 
	});
	         
	$('#buttonReq').click(function(event) {
		$('#data').empty();
		var value = $('#number').val();
		fetch('/read/'+value)
			.then(response=>response.json())
			.then((data)=>{
				var read_x = [];
				var read_y = [];
				for(var i = 0; i<data.rows.length; i++){
					readFile(data.rows[i].index,data.rows[i].value)
					read_x.push(parseFloat(data.rows[i].index));
					read_y.push(parseFloat(data.rows[i].value));
				}
				setReadGraph(read_x,read_y);
			});
		return false; 
	});
	
	$('#relay1').click(function(event) {
		onRelay(1);
	});

	$('#relay2').click(function(event) {
		onRelay(2);
	});
	$('#relay3').click(function(event) {
		onRelay(3);
	});
	$('#relay4').click(function(event) {
		onRelay(4);
	});
	
	$('#open').click(function(event) {
		openSocket();
	});
	$('#start').click(function(event) {
		startListening();
	});
	$('#stop').click(function(event) {
		stopListening();
	});
	$('#close').click(function(event) {
		closeSocket();
	});
}


function initTabs(){
	$( "#tabs" ).tabs(
		//{event: "mouseover"}
	);
}

function onReady(){
	initTabs();
	createGauge();
	initButtons();
}

$(document).ready(onReady); 















